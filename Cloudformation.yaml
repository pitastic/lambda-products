---

AWSTemplateFormatVersion: "2010-09-09"
Description: Lambda Example

Parameters:
  FunctionName: 
    Description: Name of the Lambda Function
    Type: String
  FunctionRuntime:
    Description: Runtime of the function
    Type: String
  FunctionHandler:
    Description: Handler of the function
    Type: String
  FunctionDescription:
    Description: "Description of the function"
    Type: String
  S3BucketSource:
    Description: Name of the S3 Bucket keeping the source code of the lambda function
    Type: String
  StripePrivateKey:
    Description: private key for the stripe environmnt
    Type: String

Resources:
  # create an s3 bucket which will be used to contain the code zips
  LambdaCodeBucket:
    Type: "AWS::S3::Bucket"
    Properties: 
      BucketName: !Ref S3BucketSource
      AccessControl: Private

  # create the lambdahelper execution role and policies
  # the lambda helper function is used to automatically create
  # an empty zip file so we can create the lambda 
  # why a s3 zip file instead of inline you ask ?
  # inline code (ZipFile: | ...) is only supported for python and nodejs
  # this cf template should be as general as possible so we can also deploy java 
  # and go lambdas
  LambdaHelperExecutionRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"

  # https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_s3_rw-bucket.html
  LambdaHelperPolicy: 
    Type: "AWS::IAM::Policy"
    Properties: 
      PolicyName: "root"
      PolicyDocument: 
        Version: "2012-10-17"
        Statement: 
          - 
            Effect: "Allow"
            Action: "s3:ListBucket"
            Resource: !Sub "arn:aws:s3:::${S3BucketSource}"
          - 
            Effect: "Allow"
            Action: 
              - "s3:*"
            Resource: !Sub "arn:aws:s3:::${S3BucketSource}/*"
      Roles: 
        - 
          Ref: "LambdaHelperExecutionRole"

  # small helper function to create an empty "init.zip" file in the s3 bucket
  LambdaHelperFunction:
    Type: "AWS::Lambda::Function"
    DependsOn: LambdaCodeBucket
    Properties:
      Description: "Lambda Helper Function to create initial zip file in bucket"
      FunctionName: !Sub ${FunctionName}Helper
      Runtime: "python3.6"
      Handler: "index.save_to_bucket"
      Role: !GetAtt LambdaHelperExecutionRole.Arn
      Code:
        # upload a zip file containing an empty text file to s3 (an empty zip file is not supported)
        # 2018-08-05: https://gist.github.com/tomfa/7bb519a34262353087a83712539eb6b0
        # 2018-08-05: https://stackoverflow.com/questions/25195495/how-to-create-an-empty-zip-file
        # 2018-08-05: https://stackoverflow.com/questions/43634640/cannot-add-code-to-aws-lambda-function-using-cloudformation
        # 2018-08-05: https://pprakash.me/tech/2015/12/20/sending-response-back-to-cfn-custom-resource-from-python-lambda-function/
        # 2018-08-05: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-lambda-function-code.html#cfn-lambda-function-code-cfnresponsemodule
        ZipFile: !Sub |
          import json
          import boto3
          import cfnresponse

          def save_to_bucket(event, context):
              if event['RequestType'] != 'Delete':
                  AWS_BUCKET_NAME = '${S3BucketSource}'
                  s3 = boto3.resource('s3')
                  bucket = s3.Bucket(AWS_BUCKET_NAME)
                  path = 'init.zip'
                  data = b'PK\x03\x04\n\x00\x00\x00\x00\x00Yc\x05M\x07\xc7\xb6\xae\x06\x00\x00\x00\x06\x00\x00\x00\x05\x00\x1c\x00emptyUT\t\x00\x03\xe9\xd0f[\xe7\xd0f[ux\x0b\x00\x01\x04\xf5\x01\x00\x00\x04\x00\x00\x00\x00empty\nPK\x01\x02\x1e\x03\n\x00\x00\x00\x00\x00Yc\x05M\x07\xc7\xb6\xae\x06\x00\x00\x00\x06\x00\x00\x00\x05\x00\x18\x00\x00\x00\x00\x00\x01\x00\x00\x00\xa4\x81\x00\x00\x00\x00emptyUT\x05\x00\x03\xe9\xd0f[ux\x0b\x00\x01\x04\xf5\x01\x00\x00\x04\x00\x00\x00\x00PK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x00K\x00\x00\x00E\x00\x00\x00\x00\x00'
                  bucket.put_object(
                      Key=path,
                      Body=data,
                  )
              cfnresponse.send(event, context, cfnresponse.SUCCESS, {'Data':''}, "arn:aws:fake:${FunctionName}HelperZipFile")


  # with the lambda helper function in place we create a custom resource which will call the lambda 
  # and create the zip file
  # https://community.alfresco.com/community/platform/blog/2016/10/13/how-a-lambda-backed-custom-resource-saved-the-day
  LambdaCodeBucketZip:
    Type: Custom::LambdaDependency
    DependsOn: LambdaHelperFunction
    Properties:  
      ServiceToken: !Sub "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:${FunctionName}Helper"

  # create the lambda execution role and policies for the lambda containing our function code
  LambdaExecutionRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"

  # create the lambda function with the empty zip file
  # the code can be updated with the makefile targets
  LambdaFunction:
    Type: "AWS::Lambda::Function"
    DependsOn: LambdaCodeBucketZip
    Properties:
      Description: !Ref FunctionDescription
      FunctionName: !Ref FunctionName
      Runtime: !Ref FunctionRuntime
      Handler: !Ref FunctionHandler
      Role: !GetAtt LambdaExecutionRole.Arn
      Environment:
        Variables:
          STRIPE_PRIVATE_KEY: !Ref StripePrivateKey
      Code:
        S3Bucket: !Ref S3BucketSource
        S3Key: init.zip
  
  # create lambda aliases for dev, staging and production releases
  LambdaAliasDevelopment:
    Type: AWS::Lambda::Alias
    DependsOn: LambdaFunction
    Properties:     
      Description: "development version of the function"         
      FunctionName: !Ref FunctionName
      FunctionVersion: "$LATEST"
      Name: development

  LambdaAliasStaging:
    Type: AWS::Lambda::Alias
    DependsOn: LambdaFunction
    Properties:     
      Description: "staging version of the function"         
      FunctionName: !Ref FunctionName
      FunctionVersion: "$LATEST"
      Name: staging

  LambdaAliasProduction:
    Type: AWS::Lambda::Alias
    DependsOn: LambdaFunction
    Properties:     
      Description: "production version of the function"         
      FunctionName: !Ref FunctionName
      FunctionVersion: "$LATEST"
      Name: production

Outputs:
  LambdaArn:
    Description: Arn of the Lambda function
    Value: !GetAtt LambdaFunction.Arn
    Export:
      Name: !Sub "${FunctionName}Arn"
  LambdaAliasDevelopmentArn:
    Description: Arn of the Development Alias
    Value: !Ref LambdaAliasDevelopment
    Export:
      Name: !Sub "${FunctionName}AliasDevelopmentArn"
  LambdaAliasStagingArn:
    Description: Arn of the Development Alias
    Value: !Ref LambdaAliasStaging
    Export:
      Name: !Sub "${FunctionName}AliasStagingArn"
  LambdaAliasProductionArn:
    Description: Arn of the Development Alias
    Value: !Ref LambdaAliasProduction
    Export:
      Name: !Sub "${FunctionName}AliasProductionArn"
  LambdaName:
    Description: Name of the Lambda function
    Value: !Ref FunctionName
    Export:
      Name: !Sub "${FunctionName}Name"
