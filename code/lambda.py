#!/usr/bin/env python

"""
	Access the stripe api and retrieve products
"""

import stripe
import json
import os

def response(message, status_code):
    return {
        'statusCode': str(status_code),
        'body': json.dumps(message),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
            },
        }

# quick n dirty get a list of products from stripe
def handler(event, context):
    try:
        # get the api key from the lambdas env 
        # @TODO: migrate to parameter store with kms
        api_key=os.getenv("STRIPE_PRIVATE_KEY")
        if not api_key:
            return response({'message': 'Bad request: Stripe Api key missing'}, 400)

        # connect the stripe api
        stripe.api_key = api_key

        # check if there is an id defined or not
        productid = None
        if 'pathParameters' in event:
            pathparameters = event['pathParameters']
            if pathparameters is not None:
                if 'id' in pathparameters:
                    productid = pathparameters['id']

        # check if we have an id defined
        if productid is not None:
            # if an id is specified try to get a single product 
            # return the product also if its disabled
            return response(((stripe.Product.retrieve(productid)).__dict__)['_previous'],200)
        else:
            # if no id is specified we just get a list of all product
            # filter by active and return the unsorted list
            products = []
            for p in stripe.Product.list().data:
                if p.active:
                    products.append((p.__dict__)['_previous'])

            return response(products,200)

    except Exception as e:
        print (e)
        return response({'message': 'Internal Server Error'}, 500)
