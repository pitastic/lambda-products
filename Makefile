####
#
# makefile to deploy an example aws lambda
#
####

#
# VARIABLES
#

# name of the cloudformation stack to create
STACK_NAME?=pitastic-lambda-products
# name of the lambda function to create
FUNCTION_NAME?=Products
# description of the function
FUNCTION_DESCRIPTION?='retrieve products from stripe'
# runtime of the function 
FUNCTION_RUNTIME?=python3.6
# handler for the function
FUNCTION_HANDLER?=lambda.handler
# STRIPE API Key
STRIPE_PRIVATE_KEY=$(shell grep 'STRIPE_PRIVATE_KEY' .env | cut -f2 -d'=')

# name of the s3 bucket which is used to store the code as zip
S3_BUCKET?=$(shell echo "${STACK_NAME}-source" | tr A-Z a-z)
# timestamp used to set the name of the code zip
TIMESTAMP:=$(shell date +%s)
# name of the code zip to generate
ZIP?=code.${TIMESTAMP}.zip

# general shortcuts
DOCKER=docker run --rm -ti --env-file ${CURDIR}/.env -v ${CURDIR}:/data -w /data
BASH=${DOCKER} --entrypoint=/bin/bash piaws -c
JQ=${DOCKER} --entrypoint=/usr/bin/jq piaws

#
# TARGETS
#

# setup virtualenv for python development
# http://blog.bottlepy.org/2012/07/16/virtualenv-and-makefiles.html
venv: venv/bin/activate
venv/bin/activate: requirements.txt
	test -d venv || virtualenv -p /usr/local/bin/python3 venv
	venv/bin/pip install -Ur requirements.txt
	touch venv/bin/activate

# build docker image
docker-image:
	docker build -t piaws .

# initialize the aws lambda function
cloudformation-lambda:
	@echo "execute cloudformation deployment"
	@${BASH} "aws cloudformation deploy \
		--stack-name ${STACK_NAME} --template-file Cloudformation.yaml \
		--capabilities CAPABILITY_IAM \
		--no-fail-on-empty-changeset \
		--parameter-overrides \
			FunctionName=${FUNCTION_NAME} \
			FunctionDescription=${FUNCTION_DESCRIPTION} \
			FunctionRuntime=${FUNCTION_RUNTIME} \
			FunctionHandler=${FUNCTION_HANDLER} \
			S3BucketSource=${S3_BUCKET} \
			StripePrivateKey=${STRIPE_PRIVATE_KEY}"

# run local tests
unit-tests: venv
	@echo "execute unit tests"

# package the code into a zip file
# https://docs.aws.amazon.com/lambda/latest/dg/lambda-python-how-to-create-deployment-package.html
create-zip: unit-tests
	@echo "create zip from code"
	@cd code/ ;\
		zip -r9 ../${ZIP} .
	@echo "add virtual env site packages to zip"
	cd venv/lib/python3*/site-packages/ ;\
		zip -r9 ../../../../${ZIP} . -x setuptools\* -x pip\*

# upload the code to the s3 bucket
upload-zip: create-zip
	@echo "upload zip ${ZIP} to ${S3_BUCKET}"
	@${BASH} "aws s3 cp ${ZIP} s3://${S3_BUCKET}"

# update the lambda code
update-lambda: upload-zip
	@echo "update lambda ${FUNCTION_NAME}"
	@${BASH} "aws lambda update-function-code \
		--function-name ${FUNCTION_NAME} \
		--s3-bucket ${S3_BUCKET} \
		--s3-key ${ZIP}"

# run integration tests
integration-tests: update-lambda
	@echo "execute integration tests"

# publish the latest version
publish-lambda: integration-tests
	@echo "publish uploaded lambda version"
	@${BASH} "aws lambda publish-version \
		--function-name ${FUNCTION_NAME} \
		--description ${TIMESTAMP}"

	@echo "retrieve all lambda versions"
	@${BASH} "aws lambda list-versions-by-function \
		--function-name ${FUNCTION_NAME} > .lambda.versions"

	@echo "read the latest lambda version"
	@${JQ} -r '.Versions[] | select (.Description == "${TIMESTAMP}") | .Version' .lambda.versions > .lambda.latest

# set aliases to latest version
# usually we would replace the timestamp naming with proper build numbers from a ci system
# and then update one environment after the other
push-to-development:
	@echo "update development alias with latest version" 
	@${BASH} "aws lambda update-alias \
		--function-name ${FUNCTION_NAME} \
		--name development \
		--description ${TIMESTAMP} \
		--function-version $(shell cat .lambda.latest)"

push-to-production:
	@echo "update production alias with latest version" 
	@${BASH} "aws lambda update-alias \
		--function-name ${FUNCTION_NAME} \
		--name production \
		--description ${TIMESTAMP} \
		--function-version $(shell cat .lambda.latest)"

push-to-all: push-to-development push-to-production


# cleanup local zip files
clean:
	-rm code.*.zip
